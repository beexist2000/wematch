package com.ljw.wematch.advice;

import com.ljw.wematch.enums.ResultCode;
import com.ljw.wematch.exception.*;
import com.ljw.wematch.model.common.CommonResult;
import com.ljw.wematch.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    /**
     * 기본 탈출구 : 실패함
     *
     * @param request http 요청
     * @param e 예외 객체
     * @return ResponseService 클래스에 실패 정보 설정
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CAccessDeniedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAccessDeniedException e) {
        return ResponseService.getFailResult(ResultCode.ACCESS_DENIED);
    }

    @ExceptionHandler(CAuthenticationEntryPointException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAuthenticationEntryPointException e) {
        return ResponseService.getFailResult(ResultCode.AUTHENTICATION_ENTRY_POINT);
    }

    @ExceptionHandler(CUsernameSignInFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CUsernameSignInFailedException e) {
        return ResponseService.getFailResult(ResultCode.USERNAME_SIGN_IN_FAILED);
    }

    @ExceptionHandler(CUsernameValidFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CUsernameValidFailedException e) {
        return ResponseService.getFailResult(ResultCode.USERNAME_VALID_FAILED);
    }

    @ExceptionHandler(CPasswordNotEqualReException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CPasswordNotEqualReException e) {
        return ResponseService.getFailResult(ResultCode.PASSWORD_NOT_EQUAL_RE);
    }

    @ExceptionHandler(CUsernameDuplicationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CUsernameDuplicationException e) {
        return ResponseService.getFailResult(ResultCode.USERNAME_DUPLICATION);
    }

    @ExceptionHandler(CUserNotExistException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CUserNotExistException e) {
        return ResponseService.getFailResult(ResultCode.USER_NOT_EXIST);
    }

    @ExceptionHandler(CPasswordNewNotEqualException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CPasswordNewNotEqualException e) {
        return ResponseService.getFailResult(ResultCode.PASSWORD_NEW_NOT_EQUAL);
    }

    @ExceptionHandler(CPasswordNewDuplicationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CPasswordNewDuplicationException e) {
        return ResponseService.getFailResult(ResultCode.PASSWORD_NEW_DUPLICATION);
    }

    @ExceptionHandler(CAdminDeleteFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CAdminDeleteFailedException e) {
        return ResponseService.getFailResult(ResultCode.ADMIN_DELETE_FAILED);
    }

    @ExceptionHandler(CNicknameDuplicationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNicknameDuplicationException e) {
        return ResponseService.getFailResult(ResultCode.NICKNAME_DUPLICATION);
    }

    /**
     * 커스텀 탈출구 : 데이터가 존재하지 않음
     *
     * @param request http 요청
     * @param e 예외 객체
     * @return ResponseService 클래스에 실패 정보 설정
     */
    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CAcceptStateNotYetException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CAcceptStateNotYetException e) {
        return ResponseService.getFailResult(ResultCode.ACCEPT_STATE_NOT_YET);
    }

    @ExceptionHandler(CApplySelfFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CApplySelfFailedException e) {
        return ResponseService.getFailResult(ResultCode.APPLY_SELF_FAILED);
    }

    @ExceptionHandler(CApplyExistFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CApplyExistFailedException e) {
        return ResponseService.getFailResult(ResultCode.APPLY_EXIST_FAILED);
    }

    @ExceptionHandler(CProcessingStateAlreadyException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CProcessingStateAlreadyException e) {
        return ResponseService.getFailResult(ResultCode.PROCESSING_STATE_ALREADY_FINISH);
    }
}
