package com.ljw.wematch.entity;

import com.ljw.wematch.interfaces.CommonModelBuilder;
import com.ljw.wematch.lib.CommonDate;
import com.ljw.wematch.model.match.MatchRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Match {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false, length = 30)
    @ApiModelProperty(notes = "경기 장소")
    private String placeName;

    @Column(nullable = false, length = 40)
    @ApiModelProperty(notes = "경기 주소")
    private String addressName;

    @Column(nullable = false, length = 30)
    @ApiModelProperty(notes = "글 제목")
    private String title;

    @Column(nullable = false, columnDefinition = "TEXT")
    @ApiModelProperty(notes = "글 내용")
    private String contents;

    @Column(nullable = false)
    @ApiModelProperty(notes = "경기일")
    private LocalDate sportDate;

    @Column(nullable = false)
    @ApiModelProperty(notes = "경도")
    private Double posX;

    @Column(nullable = false)
    @ApiModelProperty(notes = "위도")
    private Double posY;

    @Column(nullable = false)
    @ApiModelProperty(notes = "총 신청 인원")
    private Long personCount;

    @Column(nullable = false)
    @ApiModelProperty(notes = "활성화 여부")
    private Boolean isEnabled;

    @Column(nullable = false)
    @ApiModelProperty(notes = "등록날짜")
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    @ApiModelProperty(notes = "수정날짜")
    private LocalDateTime dateUpdate;

    public void putIsEnable() {
        this.isEnabled = false;
        this.dateUpdate = CommonDate.getNowTime();
    }

    public void putData(MatchRequest request) {
        this.placeName = request.getPlaceName();
        this.addressName = request.getAddressName();
        this.title = request.getTitle();
        this.contents = request.getContents();
        this.sportDate = request.getSportDate();
        this.posX = request.getPosX();
        this.posY = request.getPosY();
        this.dateUpdate = CommonDate.getNowTime();
    }

    private Match(MatchBuilder builder) {
        this.member = builder.member;
        this.placeName = builder.placeName;
        this.addressName = builder.addressName;
        this.title = builder.title;
        this.contents = builder.contents;
        this.sportDate = builder.sportDate;
        this.posX = builder.posX;
        this.posY = builder.posY;
        this.personCount = builder.personCount;
        this.isEnabled = builder.isEnabled;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class MatchBuilder implements CommonModelBuilder<Match> {
        private final Member member;
        private final String placeName;
        private final String addressName;
        private final String title;
        private final String contents;
        private final LocalDate sportDate;
        private final Double posX;
        private final Double posY;
        private final Long personCount;
        private final Boolean isEnabled;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;


        public MatchBuilder(Member member, MatchRequest request) {
            this.member = member;
            this.placeName = request.getPlaceName();
            this.addressName = request.getAddressName();
            this.title = request.getTitle();
            this.contents = request.getContents();
            this.sportDate = request.getSportDate();
            this.posX = request.getPosX();
            this.posY = request.getPosY();
            this.personCount = 0L;
            this.isEnabled = true;
            this.dateCreate = CommonDate.getNowTime();
            this.dateUpdate = CommonDate.getNowTime();
        }

        @Override
        public Match build() {
            return new Match(this);
        }
    }
}
