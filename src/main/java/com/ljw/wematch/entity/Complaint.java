package com.ljw.wematch.entity;

import com.ljw.wematch.enums.ProcessingState;
import com.ljw.wematch.interfaces.CommonModelBuilder;
import com.ljw.wematch.lib.CommonDate;
import com.ljw.wematch.model.complaint.ComplaintCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Complaint {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false, length = 30)
    @ApiModelProperty(notes = "글 제목")
    private String title;

    @Column(nullable = false, columnDefinition = "TEXT")
    @ApiModelProperty(notes = "글 내용")
    private String contents;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    @ApiModelProperty(notes = "처리 상태")
    private ProcessingState processingState;

    @Column(nullable = false)
    @ApiModelProperty(notes = "등록 날짜")
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    @ApiModelProperty(notes = "수정 날짜")
    private LocalDateTime dateUpdate;

    @ApiModelProperty(notes = "완료 날짜")
    private LocalDateTime dateComplete;

    public void putProcessingState(ProcessingState processingState) {
        this.processingState = processingState;
        this.dateUpdate = CommonDate.getNowTime();
        if (processingState.equals(ProcessingState.COMPLETE)) {
            this.dateComplete = CommonDate.getNowTime();
        }
    }

    private Complaint(ComplaintBuilder builder) {
        this.member = builder.member;
        this.title = builder.title;
        this.contents = builder.contents;
        this.processingState = builder.processingState;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class ComplaintBuilder implements CommonModelBuilder<Complaint> {
        private final Member member;
        private final String title;
        private final String contents;
        private final ProcessingState processingState;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public ComplaintBuilder(Member member, ComplaintCreateRequest createRequest) {
            this.member = member;
            this.title = createRequest.getTitle();
            this.contents = createRequest.getContents();
            this.processingState = ProcessingState.STANDBY;
            this.dateCreate = CommonDate.getNowTime();
            this.dateUpdate = CommonDate.getNowTime();
        }

        @Override
        public Complaint build() {
            return new  Complaint(this);
        }
    }
}
