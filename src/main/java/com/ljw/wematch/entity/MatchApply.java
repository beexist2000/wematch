package com.ljw.wematch.entity;

import com.ljw.wematch.enums.AcceptState;
import com.ljw.wematch.interfaces.CommonModelBuilder;
import com.ljw.wematch.lib.CommonDate;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MatchApply {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "matchId", nullable = false)
    private Match match;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    @ApiModelProperty(notes = "수락 상태")
    private AcceptState acceptState;

    @Column(nullable = false)
    @ApiModelProperty(notes = "활성화 여부")
    private Boolean isEnabled;

    @ApiModelProperty(notes = "수락 날짜")
    private LocalDateTime dateAccept;

    @ApiModelProperty(notes = "거절 날짜")
    private LocalDateTime dateRefuse;

    @Column(nullable = false)
    @ApiModelProperty(notes = "등록 날짜")
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    @ApiModelProperty(notes = "수정 날짜")
    private LocalDateTime dateUpdate;

    public void putAcceptState(AcceptState acceptState) {
        this.acceptState = acceptState;
    }

    public void putIsEnabled() {
        this.isEnabled = false;
        this.dateUpdate = CommonDate.getNowTime();
    }

    private MatchApply(MatchApplyBuilder builder) {
        this.match = builder.match;
        this.member = builder.member;
        this.acceptState = builder.acceptState;
        this.isEnabled = builder.isEnabled;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class MatchApplyBuilder implements CommonModelBuilder<MatchApply> {
        private final Match match;
        private final Member member;
        private final AcceptState acceptState;
        private final Boolean isEnabled;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MatchApplyBuilder(Match match, Member member) {
            this.match = match;
            this.member = member;
            this.acceptState = AcceptState.YET;
            this.isEnabled = true;
            this.dateCreate = CommonDate.getNowTime();
            this.dateUpdate = CommonDate.getNowTime();
        }

        @Override
        public MatchApply build() {
            return new MatchApply(this);
        }
    }
}
