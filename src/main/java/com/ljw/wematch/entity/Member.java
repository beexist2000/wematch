package com.ljw.wematch.entity;

import com.ljw.wematch.enums.Gender;
import com.ljw.wematch.enums.MemberGroup;
import com.ljw.wematch.interfaces.CommonModelBuilder;
import com.ljw.wematch.lib.CommonDate;
import com.ljw.wematch.lib.CommonFormat;
import com.ljw.wematch.model.member.MemberCreateRequest;
import com.ljw.wematch.model.member.MemberDataUpdateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "회원 시퀀스")
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    @ApiModelProperty(notes = "사용자 권한")
    private MemberGroup memberGroup;

    @Column(nullable = false, unique = true, length = 50)
    @ApiModelProperty(notes = "사용자 아이디")
    private String username;

    @Column(nullable = false)
    @ApiModelProperty(notes = "사용자 비밀번호")
    private String password;

    @Column(nullable = false, unique = true, length = 30)
    @ApiModelProperty(notes = "닉네임")
    private String nickname;

    @Column(nullable = false, length = 20)
    @ApiModelProperty(notes = "사용자 이름")
    private String name;

    @Column(nullable = false)
    @ApiModelProperty(notes = "생년월일")
    private LocalDate birthday;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    @ApiModelProperty(notes = "성별")
    private Gender gender;

    @Column(nullable = false, length = 15)
    @ApiModelProperty(notes = "연락처")
    private String phone;

    @Column(length = 15)
    @ApiModelProperty(notes = "주종목")
    private String sportType;

    @Column(length = 40)
    @ApiModelProperty(notes = "활동 지역")
    private String location;

    @ApiModelProperty(notes = "이미지 이름")
    private String imageName;

    @ApiModelProperty(notes = "이미지 업로드시간")
    private LocalDateTime dateUploadImage;

    @Column(nullable = false)
    @ApiModelProperty(notes = "등록시간")
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    @ApiModelProperty(notes = "수정시간")
    private LocalDateTime dateUpdate;

    @ApiModelProperty(notes = "탈퇴시간")
    private LocalDateTime dateDisable;

    @Column(nullable = false)
    @ApiModelProperty(notes = "활성화 여부")
    private Boolean isEnabled;

    public void putPassword(String passwordUpdate) {
        this.password = passwordUpdate;
        this.dateUpdate = CommonDate.getNowTime();
    }

    public void putMemberData(MemberDataUpdateRequest updateRequest) {
        this.nickname = updateRequest.getNickname();
        this.phone = updateRequest.getPhone();
        this.sportType = updateRequest.getSportType();
        this.location = updateRequest.getLocation();
        this.dateUpdate = CommonDate.getNowTime();
    }

    public void putIsEnabled() {
        this.isEnabled = false;
        this.dateUpdate = CommonDate.getNowTime();
        this.dateDisable = CommonDate.getNowTime();
        this.username = this.username + "_" + CommonFormat.convertLocalDateTimeToSimpleString(this.dateDisable);
        this.nickname = this.nickname + "_" + CommonFormat.convertLocalDateTimeToSimpleString(this.dateDisable);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(memberGroup.toString()));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnabled;
    }

    private Member(MemberBuilder builder) {
        this.memberGroup = builder.memberGroup;
        this.username = builder.username;
        this.password = builder.password;
        this.nickname = builder.nickname;
        this.name = builder.name;
        this.birthday = builder.birthday;
        this.gender = builder.gender;
        this.phone = builder.phone;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
        this.isEnabled = builder.isEnabled;
    }

    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final MemberGroup memberGroup;
        private final String username;
        private final String password;
        private final String nickname;
        private final String name;
        private final LocalDate birthday;
        private final Gender gender;
        private final String phone;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;
        private final Boolean isEnabled;

        public MemberBuilder(MemberGroup memberGroup, MemberCreateRequest joinRequest) {
            this.memberGroup = memberGroup;
            this.username = joinRequest.getUsername();
            this.password = joinRequest.getPassword();
            this.nickname = joinRequest.getNickname();
            this.name = joinRequest.getName();
            this.birthday = joinRequest.getBirthday();
            this.gender = joinRequest.getGender();
            this.phone = joinRequest.getPhone();
            this.dateCreate = CommonDate.getNowTime();
            this.dateUpdate = CommonDate.getNowTime();
            this.isEnabled = true;
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
