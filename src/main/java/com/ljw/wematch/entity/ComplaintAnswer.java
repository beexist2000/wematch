package com.ljw.wematch.entity;

import com.ljw.wematch.interfaces.CommonModelBuilder;
import com.ljw.wematch.lib.CommonDate;
import com.ljw.wematch.model.complaintanswer.ComplaintAnswerCreateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ComplaintAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "complaintId", nullable = false)
    private Complaint complaint;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String contents;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    private ComplaintAnswer(ComplaintAnswerBuilder builder) {
        this.complaint = builder.complaint;
        this.member = builder.member;
        this.contents = builder.contents;
        this.dateCreate = builder.dateCreate;
    }

    public static class ComplaintAnswerBuilder implements CommonModelBuilder<ComplaintAnswer> {
        private final Complaint complaint;
        private final Member member;
        private final String contents;
        private final LocalDateTime dateCreate;


        public ComplaintAnswerBuilder(Complaint complaint, Member member, ComplaintAnswerCreateRequest createRequest) {
            this.complaint = complaint;
            this.member = member;
            this.contents = createRequest.getContents();
            this.dateCreate = CommonDate.getNowTime();
        }

        @Override
        public ComplaintAnswer build() {
            return new ComplaintAnswer(this);
        }
    }
}
