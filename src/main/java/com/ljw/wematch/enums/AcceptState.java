package com.ljw.wematch.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AcceptState {
    ACCEPT("승락")
    , REFUSE("거부")
    , YET("미처리")
    ;

    private final String name;
}
