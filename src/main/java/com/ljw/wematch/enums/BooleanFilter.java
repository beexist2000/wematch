package com.ljw.wematch.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BooleanFilter {
    Y("네", true)
    , N("아니오", false)
    , OTHER("기타", null)
    ;

    private final String name;
    private final Boolean value;
}
