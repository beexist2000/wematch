package com.ljw.wematch.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Gender {
    MAN("남자")
    , WOMAN("여자")
    , OTHER("기타")
    ;

    private final String name;
}
