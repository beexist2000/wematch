package com.ljw.wematch.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProcessingState {
    STANDBY("대기")
    , INCOMPLETE("미완료")
    , COMPLETE("완료")
    ;

    private final String name;
}
