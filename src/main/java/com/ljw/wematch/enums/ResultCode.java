package com.ljw.wematch.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    // 상황에 따른 코드와 메시지를 관리
    SUCCESS(0, "성공하였습니다.") // 성공 코드와 메시지
    , FAILED(-1, "실패하였습니다.") // 실패 코드와 메시지

    , ACCESS_DENIED(-1000, "권한이 없습니다.")
    , USERNAME_SIGN_IN_FAILED(-1001, "가입된 사용자가 아닙니다.")
    , AUTHENTICATION_ENTRY_POINT(-1002, "접근 권한이 없습니다.")
    , USERNAME_VALID_FAILED(-1003, "유효한 아이디가 아닙니다.")
    , PASSWORD_NOT_EQUAL_RE(-1004, "비밀번호가 일치하지 않습니다.")
    , USERNAME_DUPLICATION(-1005, "중복된 아이디가 존재합니다.")
    , USER_NOT_EXIST(-1006, "회원정보가 없습니다.")
    , PASSWORD_NEW_NOT_EQUAL(-1007, "새 비밀번호가 일치하지 않습니다.")
    , PASSWORD_NEW_DUPLICATION(-1008, "현재 비밀번호와 새 비밀번호가 같습니다.")
    , ADMIN_DELETE_FAILED(-1009, "최고관리자는 삭제할 수 없습니다.")
    , NICKNAME_DUPLICATION(-1010, "중복된 닉네임이 존재합니다.")

    , MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.") // 데이터를 찾을 수 없는 예외 상황에 대한 코드와 메시지

    , ACCEPT_STATE_NOT_YET(-20000, "이미 이 신청을 처리하였습니다.")
    , APPLY_SELF_FAILED(-20001, "스스로 작성한 경기에는 신청할 수 없습니다.")
    , APPLY_EXIST_FAILED(-20002, "이미 신청한 상태입니다.")

    , PROCESSING_STATE_ALREADY_FINISH(-30000, "이 건의사항은 이미 답변 완료한 상태입니다.")
    ;

    private final Integer code; // 코드의 자료 형식과 이름
    private final String msg; // 메시지의 자료 형식과 이름
}
