package com.ljw.wematch.service;

import com.ljw.wematch.entity.Complaint;
import com.ljw.wematch.entity.ComplaintAnswer;
import com.ljw.wematch.entity.Member;
import com.ljw.wematch.enums.ProcessingState;
import com.ljw.wematch.exception.CMissingDataException;
import com.ljw.wematch.exception.CProcessingStateAlreadyException;
import com.ljw.wematch.model.common.ListResult;
import com.ljw.wematch.model.complaint.ComplaintCreateRequest;
import com.ljw.wematch.model.complaint.ComplaintDetail;
import com.ljw.wematch.model.complaint.ComplaintItem;
import com.ljw.wematch.model.complaintanswer.ComplaintAnswerCreateRequest;
import com.ljw.wematch.model.complaintanswer.ComplaintAnswerItem;
import com.ljw.wematch.repository.ComplaintAnswerRepository;
import com.ljw.wematch.repository.ComplaintRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ComplaintService {
    private final ComplaintRepository complaintRepository;
    private final ComplaintAnswerRepository complaintAnswerRepository;

    /**
     * 건의사항 등록
     *
     * @param member 회원 정보
     * @param createRequest 등록할 건의사항 정보
     */
    public void setComplaint(Member member, ComplaintCreateRequest createRequest) {
        Complaint complaint = new Complaint.ComplaintBuilder(member, createRequest).build();
        complaintRepository.save(complaint);
    }

    /**
     * 건의사항 답변 등록
     *
     * @param member 회원 정보
     * @param complaintId 건의사항 시퀀스
     * @param createRequest 등록할 건의사항 답변 정보
     */
    public void setComplaintAnswer(Member member, long complaintId, ComplaintAnswerCreateRequest createRequest) {
        Complaint complaint = complaintRepository.findById(complaintId).orElseThrow(CMissingDataException::new);

        if (complaint.getProcessingState().equals(ProcessingState.COMPLETE)) throw new CProcessingStateAlreadyException();

        ComplaintAnswer complaintAnswer = new ComplaintAnswer.ComplaintAnswerBuilder(complaint, member, createRequest).build();
        complaint.putProcessingState(createRequest.getProcessingState());

        complaintRepository.save(complaint);
        complaintAnswerRepository.save(complaintAnswer);
    }

    /**
     * 내 건의사항 리스트 조회
     *
     * @param member 회원 정보
     * @param pageNum 페이지 번호
     * @param year 기준년도
     * @param month 기준월
     * @return 건의사항 리스트
     */
    public ListResult<ComplaintItem> getMyComplaints(Member member, int pageNum, int year, int month) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);

        LocalDateTime dateStart = LocalDateTime.of(year, month, 1, 0, 0, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month-1, 1);
        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        LocalDateTime dateEnd = LocalDateTime.of(year, month, maxDay, 23, 59, 59);

        Page<Complaint> originList = complaintRepository.findAllByMemberAndDateCreateGreaterThanEqualAndDateCreateLessThanEqual(
                member,
                dateStart,
                dateEnd,
                pageRequest
        );

        List<ComplaintItem> result = new LinkedList<>();

        originList.getContent().forEach(complaint -> {
            ComplaintItem complaintItem = new ComplaintItem.ComplaintItemBuilder(complaint).build();
            result.add(complaintItem);
        });

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    /**
     * 건의사항 리스트 조회
     *
     * @param pageNum 페이지 번호
     * @param year 기준년도
     * @param month 기준월
     * @return 건의사항 리스트
     */
    public ListResult<ComplaintItem> getComplaint(int pageNum, int year, int month) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);

        LocalDateTime dateStart = LocalDateTime.of(year, month, 1, 0, 0, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month-1, 1);
        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        LocalDateTime dateEnd = LocalDateTime.of(year, month, maxDay, 23, 59, 59);

        Page<Complaint> originList = complaintRepository.findAllByMember_IsEnabledAndDateCreateGreaterThanEqualAndDateCreateLessThanEqual(
                true,
                dateStart,
                dateEnd,
                pageRequest
        );

        List<ComplaintItem> result = new LinkedList<>();

        originList.getContent().forEach(complaint -> {
            ComplaintItem complaintItem = new ComplaintItem.ComplaintItemBuilder(complaint).build();
            result.add(complaintItem);
        });

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    /**
     * 건의사항 상세정보 조회
     *
     * @param complaintId 건의사항 시퀀스
     * @return 건의사항 상세정보
     */
    public ComplaintDetail getComplaintDetail(long complaintId) {
        Complaint complaint = complaintRepository.findById(complaintId).orElseThrow(CMissingDataException::new);
        return new ComplaintDetail.ComplaintDetailBuilder(complaint).build();
    }

    /**
     * 건의사항에 달린 답변 리스트 조회
     *
     * @param complaintId 건의사항 시퀀스
     * @return 건의사항 답변 리스트
     */
    public ListResult<ComplaintAnswerItem> getComplaintAnswers(long complaintId) {
        List<ComplaintAnswer> complaintAnswers = complaintAnswerRepository.findAllByComplaint_Id(complaintId);
        List<ComplaintAnswerItem> result = new LinkedList<>();

        complaintAnswers.forEach(complaintAnswer -> {
            ComplaintAnswerItem complaintAnswerItem = new ComplaintAnswerItem.ComplaintAnswerItemBuilder(complaintAnswer).build();
            result.add(complaintAnswerItem);
        });

        return ListConvertService.settingResult(result);
    }
}
