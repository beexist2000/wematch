package com.ljw.wematch.service;

import com.ljw.wematch.configure.JwtTokenProvider;
import com.ljw.wematch.entity.Member;
import com.ljw.wematch.enums.MemberGroup;
import com.ljw.wematch.exception.CMissingDataException;
import com.ljw.wematch.exception.CPasswordNotEqualReException;
import com.ljw.wematch.exception.CUserNotExistException;
import com.ljw.wematch.model.member.LoginRequest;
import com.ljw.wematch.model.member.LoginResponse;
import com.ljw.wematch.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final MemberRepository memberRepository;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;

    /**
     * 로그인
     *
     * @param memberGroup 회원 권한
     * @param loginRequest 로그인 정보
     * @param loginType 로그인한 클라이언트 종류(웹 or 앱)
     * @return 토큰 정보
     */
    public LoginResponse doLogin(MemberGroup memberGroup, LoginRequest loginRequest, String loginType) {
        Member member = memberRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CUserNotExistException::new); // 회원정보가 없습니다 던지기

        if (!member.getIsEnabled()) throw new CUserNotExistException(); // 회원탈퇴인 경우인데 DB에 남아있으므로 이걸 안 들키려면 회원정보가 없습니다 이걸로 던져야 함.
        if (!member.getMemberGroup().equals(memberGroup)) throw new CUserNotExistException(); // 일반회원이 최고관리자용으로 로그인하려거나 이런 경우이므로 메세지는 회원정보가 없습니다로 일단 던짐.
        if (!passwordEncoder.matches(loginRequest.getPassword(), member.getPassword())) throw new CPasswordNotEqualReException(); // 비밀번호가 일치하지 않습니다 던짐

        String token = jwtTokenProvider.createToken(member.getUsername(), member.getMemberGroup().toString(), loginType);

        return new LoginResponse.LoginResponseBuilder(token, member.getName()).build();
    }
}
