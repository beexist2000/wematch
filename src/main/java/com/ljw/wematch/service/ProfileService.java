package com.ljw.wematch.service;

import com.ljw.wematch.entity.Member;
import com.ljw.wematch.exception.CAccessDeniedException;
import com.ljw.wematch.exception.CMissingDataException;
import com.ljw.wematch.exception.CUserNotExistException;
import com.ljw.wematch.model.member.ProfileResponse;
import com.ljw.wematch.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProfileService {
    private final MemberRepository memberRepository;

    /**
     * 회원 정보 호출
     *
     * @return 회원 정보
     */
    public Member getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Member member = memberRepository.findByUsername(username).orElseThrow(CUserNotExistException::new); // 회원정보가 없습니다 던지기
        if (!member.getIsEnabled()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return member;
    }

    /**
     * 프로필 정보 조회
     *
     * @return 프로필 정보
     */
    public ProfileResponse getProfile() {
        Member member = getMemberData();
        return new ProfileResponse.ProfileResponseBuilder(member).build();
    }
}
