package com.ljw.wematch.service;

import com.ljw.wematch.entity.Match;
import com.ljw.wematch.entity.MatchApply;
import com.ljw.wematch.entity.Member;
import com.ljw.wematch.enums.AcceptState;
import com.ljw.wematch.enums.MemberGroup;
import com.ljw.wematch.exception.CAcceptStateNotYetException;
import com.ljw.wematch.exception.CApplyExistFailedException;
import com.ljw.wematch.exception.CApplySelfFailedException;
import com.ljw.wematch.exception.CMissingDataException;
import com.ljw.wematch.model.common.ListResult;
import com.ljw.wematch.model.match.MatchDetail;
import com.ljw.wematch.model.match.MatchItem;
import com.ljw.wematch.model.match.MatchRequest;
import com.ljw.wematch.model.matchapply.MatchApplyDetail;
import com.ljw.wematch.model.matchapply.MatchApplyItem;
import com.ljw.wematch.repository.MatchApplyRepository;
import com.ljw.wematch.repository.MatchRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MatchService {
    private final MatchRepository matchRepository;
    private final MatchApplyRepository matchApplyRepository;

    @PersistenceContext
    EntityManager entityManager;

    /**
     * 경기 등록
     *
     * @param member 회원 정보
     * @param request 등록할 경기 정보
     */
    public void setMatch(Member member, MatchRequest request) {
        Match match = new Match.MatchBuilder(member, request).build();
        matchRepository.save(match);
    }

    /**
     * 내 경기 리스트 조회
     *
     * @param member 회원 정보
     * @param pageNum 페이지 번호
     * @param year 기준년도
     * @param month 기준월
     * @return 경기 리스트
     */
    public ListResult<MatchItem> getMyMatches(Member member, int pageNum, int year, int month) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);

        LocalDateTime dateStart = LocalDateTime.of(year, month, 1, 0, 0, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month-1, 1);
        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        LocalDateTime dateEnd = LocalDateTime.of(year, month, maxDay, 23, 59, 59);

        Page<Match> originList = matchRepository.findAllByMemberAndIsEnabledAndDateCreateGreaterThanEqualAndDateCreateLessThanEqual(
                member,
                true,
                dateStart,
                dateEnd,
                pageRequest
        );

        List<MatchItem> result = new LinkedList<>();

        originList.getContent().forEach(match -> {
            long peopleCountStandBy = matchApplyRepository.countByMatchAndAcceptState(match, AcceptState.YET);
            MatchItem matchItem = new MatchItem.MatchItemBuilder(match, 0L, peopleCountStandBy).build();
            result.add(matchItem);
        });

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    /**
     * 경기 리스트 조회
     *
     * @param addressName 주소 키워드
     * @param title 글 제목
     * @param pageNum 페이지 번호
     * @param dateStart 조회 시작일
     * @param dateEnd 조회 마지막일
     * @return 경기 리스트
     */
    public ListResult<MatchItem> getMatches(String addressName, String title, int pageNum, LocalDate dateStart, LocalDate dateEnd) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);

        LocalDateTime dateStartTime = LocalDateTime.of(dateStart.getYear(), dateStart.getMonthValue(), dateStart.getDayOfMonth(), 0, 0, 0);
        LocalDateTime dateEndTime = LocalDateTime.of(dateEnd.getYear(), dateEnd.getMonthValue(), dateEnd.getDayOfMonth(), 23, 59, 59);

        Page<Match> originList = getData(pageRequest, addressName, title, dateStartTime, dateEndTime);

        List<MatchItem> result = new LinkedList<>();

        originList.getContent().forEach(match -> {
            long peopleCountAccept = matchApplyRepository.countByMatchAndAcceptState(match, AcceptState.ACCEPT);
            MatchItem matchItem = new MatchItem.MatchItemBuilder(match, peopleCountAccept, 0L).build();
            result.add(matchItem);
        });

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    /**
     * 페이징 처리하여 가공 안 된 원본 경기 리스트 데이터 호출
     *
     * @param pageable 페이징 관련 정보
     * @param addressName 주소
     * @param title 글 제목
     * @param dateStart 조회 시작일
     * @param dateEnd 조회 마지막일
     * @return 원본 경기 리스트 데이터
     */
    private Page<Match> getData(Pageable pageable, String addressName, String title, LocalDateTime dateStart, LocalDateTime dateEnd) {
        // Criteria 라는 단어 자체가 기준, 조건 이라는 뜻임.
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder(); // 조건을 넣을 수 있게 해주는 빌더를 가져옴.
        CriteriaQuery<Match> criteriaQuery = criteriaBuilder.createQuery(Match.class); // 기본 쿼리 생성(member select)

        // ※ 실제로는 여기에 기재된 엔티티들만 잘 설정하면 됨. ※
        Root<Match> root = criteriaQuery.from(Match.class); // 대장 엔티티를 설정해 줌.

        List<Predicate> predicates = new LinkedList<>(); // 검색 조건들을 넣을 리스트를 생성함.
        // ※ 실제로는 여기에 기재된 검색 조건들만 잘 기재하면 됨. ※
        // StringUtils.length 를 쓰는 이유. (build.gradle에 새로운 패키지 추가해놨음.)
        // String에 글자수를 가져와라 라고 length 를 쓰게 되면 null값의 경우 에러를 내뱉음. (nullpoint exception)
        // 하지만 common-lang3 패키지의 StringUtils.length 를 쓰게 되면 null값을 넣어도 에러를 뱉지 않고 숫자 0을 줌.

        if (StringUtils.length(addressName) > 0) predicates.add(criteriaBuilder.like(root.get("addressName"), "%" + addressName + "%")); // like 조건 추가
        if (StringUtils.length(title) > 0) predicates.add(criteriaBuilder.like(root.get("title"), "%" + title + "%")); // like 조건 추가

        // 영어단어를 잘 읽고 아래 조건들은 말로 해석하면 됨.
        predicates.add(criteriaBuilder.equal(root.get("isEnabled"), true));
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("dateCreate"), dateStart));
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("dateCreate"), dateEnd));
        // ※ 실제로는 여기까지 기재된 검색 조건들만 잘 기재하면 됨. ※

        Predicate[] predArray = new Predicate[predicates.size()]; // 총 where 조건의 갯수를 구함.
        predicates.toArray(predArray); // 리스트를 배열로 변환
        criteriaQuery.where(predArray); // 기본쿼리에 where 문을 생성해서 붙임.

        TypedQuery<Match> query = entityManager.createQuery(criteriaQuery); // 데이터베이스에 쿼리를 날림.

        int totalRows = query.getResultList().size(); // 총 데이터 개수를 가져옴.

        // 사용자한테 제공 될 결과데이터들을 가져옴.
        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize()); // 데이터를 가져오는데 시작지점
        query.setMaxResults(pageable.getPageSize()); // 데이터를 가져오는데 종료지점

        return new PageImpl<>(query.getResultList(), pageable, totalRows); // 페이징을 구현한 데이터를 반환함. (현재 선택된 페이지의 데이터들, 페이징 객체, 총 데이터 수)
    }

    /**
     * 경기 상세정보 조회
     *
     * @param member 회원 정보
     * @param matchId 경기 시퀀스
     * @return 경기 상세정보
     */
    public MatchDetail getMatch(Member member, long matchId) {
        Match match = matchRepository.findById(matchId).orElseThrow(CMissingDataException::new);
        if (!match.getIsEnabled()) throw new CMissingDataException();

        boolean isMine = match.getMember().equals(member);

        return new MatchDetail.MatchDetailBuilder(match, isMine).build();
    }

    /**
     * 경기 정보 수정
     *
     * @param matchId 경기 시퀀스
     * @param request 수정할 경기 정보
     */
    public void putMatch(long matchId, MatchRequest request) {
        Match match = matchRepository.findById(matchId).orElseThrow(CMissingDataException::new);
        match.putData(request);
        matchRepository.save(match);
    }

    /**
     * 경기 신청 등록
     *
     * @param member 회원 정보
     * @param matchId 경기 시퀀스
     */
    public void setMatchApply(Member member, long matchId) {
        Match match = matchRepository.findById(matchId).orElseThrow(CMissingDataException::new);
        if (match.getMember().equals(member)) throw new CApplySelfFailedException();
        if (!match.getIsEnabled()) throw new CMissingDataException();

        Optional<MatchApply> matchApplyCheck = matchApplyRepository.findByMemberAndMatch_IdAndIsEnabled(member, matchId, true);
        if (matchApplyCheck.isPresent()) throw new CApplyExistFailedException();

        MatchApply matchApply = new MatchApply.MatchApplyBuilder(match, member).build();
        matchApplyRepository.save(matchApply);
    }

    /**
     * 내 경기 신청 리스트 조회
     *
     * @param member 회원 정보
     * @param pageNum 페이지 번호
     * @param year 기준년도
     * @param month 기준월
     * @return 경기 신청 리스트
     */
    public ListResult<MatchApplyItem> getMyMatchApplies(Member member, int pageNum, int year, int month) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);

        LocalDateTime dateStart = LocalDateTime.of(year, month, 1, 0, 0, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month-1, 1);
        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        LocalDateTime dateEnd = LocalDateTime.of(year, month, maxDay, 23, 59, 59);

        Page<MatchApply> originList = matchApplyRepository.findAllByMemberAndIsEnabledAndDateCreateGreaterThanEqualAndDateCreateLessThanEqual(
                member,
                true,
                dateStart,
                dateEnd,
                pageRequest
        );

        List<MatchApplyItem> result = new LinkedList<>();

        originList.getContent().forEach(matchApply -> {
            MatchApplyItem matchApplyItem = new MatchApplyItem.MatchApplyItemBuilder(matchApply).build();
            result.add(matchApplyItem);
        });

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    /**
     * 경기 신청 리스트 조회
     *
     * @param matchId 경기 시퀀스
     * @param pageNum 페이지 번호
     * @return 경기 신청 리스트
     */
    public ListResult<MatchApplyItem> getMatchApplies(long matchId, int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);

        Page<MatchApply> originList = matchApplyRepository.findAllByMatchIdAndIsEnabled(matchId, true, pageRequest);

        List<MatchApplyItem> result = new LinkedList<>();

        originList.getContent().forEach(matchApply -> {
            MatchApplyItem matchApplyItem = new MatchApplyItem.MatchApplyItemBuilder(matchApply).build();
            result.add(matchApplyItem);
        });

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    /**
     * 경기 신청 상세정보 조회
     *
     * @param matchApplyId 경기 신청 시퀀스
     * @return 경기 신청 상세정보
     */
    public MatchApplyDetail getMatchApply(long matchApplyId) {
        MatchApply matchApply = matchApplyRepository.findById(matchApplyId).orElseThrow(CMissingDataException::new);
        if (!matchApply.getIsEnabled()) throw new CMissingDataException();

        return new MatchApplyDetail.MatchApplyDetailBuilder(matchApply).build();
    }

    /**
     * 경기 신청 처리
     *
     * @param matchApplyId 경기 신청 시퀀스
     * @param acceptState 수락 여부
     */
    public void putMatchApplyState(long matchApplyId, AcceptState acceptState) {
        MatchApply matchApply = matchApplyRepository.findById(matchApplyId).orElseThrow(CMissingDataException::new);

        if (!matchApply.getIsEnabled()) throw new CMissingDataException();
        if (!matchApply.getAcceptState().equals(AcceptState.YET)) throw new CAcceptStateNotYetException();

        matchApply.putAcceptState(acceptState);
        matchApplyRepository.save(matchApply);
    }

    /**
     * 경기 취소
     *
     * @param matchId 경기 시퀀스
     */
    public void delMatch(long matchId) {
        Match match = matchRepository.findById(matchId).orElseThrow(CMissingDataException::new);

        if (!match.getIsEnabled()) throw new CMissingDataException();

        List<MatchApply> matchApplies = matchApplyRepository.findAllByMatchIdAndIsEnabled(matchId, true);
        matchApplies.forEach(matchApply -> {
            matchApply.putIsEnabled();
            matchApplyRepository.save(matchApply);
        });

        match.putIsEnable();
        matchRepository.save(match);
    }

    /**
     * 경기 신청 취소
     *
     * @param matchApplyId 경기 신청 시퀀스
     */
    public void delMatchApply(long matchApplyId) {
        MatchApply matchApply = matchApplyRepository.findById(matchApplyId).orElseThrow(CMissingDataException::new);

        if (!matchApply.getIsEnabled()) throw new CMissingDataException();

        matchApply.putIsEnabled();
        matchApplyRepository.save(matchApply);
    }
}
