package com.ljw.wematch.service;

import com.ljw.wematch.lib.CommonDate;
import com.ljw.wematch.model.StatisticsResponse;
import com.ljw.wematch.repository.ComplaintRepository;
import com.ljw.wematch.repository.MatchRepository;
import com.ljw.wematch.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class StatisticsService {
    private final MemberRepository memberRepository;
    private final ComplaintRepository complaintRepository;
    private final MatchRepository matchRepository;

    /**
     * 오늘의 통계 자료 조회
     *
     * @return 오늘의 통계 자료
     */
    public StatisticsResponse getNowStatistics() {
        LocalDate now = CommonDate.getNowDate();
        LocalDateTime nowStart = LocalDateTime.of(now.getYear(), now.getMonthValue(), now.getDayOfMonth(), 0, 0, 0);
        LocalDateTime nowEnd = LocalDateTime.of(now.getYear(), now.getMonthValue(), now.getDayOfMonth(), 23, 59, 59);

        long countNowMemberNew = memberRepository.countByDateCreateGreaterThanEqualAndDateCreateLessThanEqualAndIsEnabled(nowStart, nowEnd, true);
        long countNowComplaintNew = complaintRepository.countByDateCreateGreaterThanEqualAndDateCreateLessThanEqual(nowStart, nowEnd);
        long countNowMatchNew = matchRepository.countByDateCreateGreaterThanEqualAndDateCreateLessThanEqualAndIsEnabled(nowStart, nowEnd, true);

        return new StatisticsResponse.StatisticsResponseBuilder(countNowMemberNew, countNowComplaintNew, countNowMatchNew).build();
    }
}
