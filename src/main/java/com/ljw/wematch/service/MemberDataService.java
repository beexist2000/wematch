package com.ljw.wematch.service;

import com.ljw.wematch.entity.Member;
import com.ljw.wematch.enums.Gender;
import com.ljw.wematch.enums.MemberGroup;
import com.ljw.wematch.exception.*;
import com.ljw.wematch.lib.CommonCheck;
import com.ljw.wematch.lib.CommonDate;
import com.ljw.wematch.model.common.ListResult;
import com.ljw.wematch.model.member.MemberAdminListItem;
import com.ljw.wematch.model.member.MemberCreateRequest;
import com.ljw.wematch.model.member.MemberDataUpdateRequest;
import com.ljw.wematch.model.member.MemberPasswordUpdateRequest;
import com.ljw.wematch.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;

    @PersistenceContext
    EntityManager entityManager;

    /**
     * 서버 시작시 관리자 계정 생성
     */
    public void setFirstMember() {
        String username = "superadmin";
        String password = "12345678";
        boolean isSuperAdmin = isNewUsername(username);

        if (isSuperAdmin) {
            MemberCreateRequest createRequest = new MemberCreateRequest();
            createRequest.setUsername(username);
            createRequest.setPassword(password);
            createRequest.setPasswordRe(password);
            createRequest.setName("최고관리자");
            createRequest.setNickname("최고관리자");
            createRequest.setGender(Gender.OTHER);
            createRequest.setBirthday(CommonDate.getNowDate());
            createRequest.setPhone("000-0000-0000");

            setMember(MemberGroup.ROLE_ADMIN, createRequest);
        }
    }

    /**
     * 회원 등록
     *
     * @param memberGroup 회원 권한
     * @param createRequest 등록할 회원 정보
     */
    public void setMember(MemberGroup memberGroup, MemberCreateRequest createRequest) {
        if (!CommonCheck.checkUsername(createRequest.getUsername())) throw new CUsernameValidFailedException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!createRequest.getPassword().equals(createRequest.getPasswordRe())) throw new CPasswordNotEqualReException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(createRequest.getUsername())) throw new CUsernameDuplicationException(); // 중복된 아이디가 존재합니다 던지기
        if (!isNewNickname(createRequest.getNickname())) throw new CNicknameDuplicationException();

        createRequest.setPassword(passwordEncoder.encode(createRequest.getPassword()));

        Member member = new Member.MemberBuilder(memberGroup, createRequest).build();
        memberRepository.save(member);
    }

    /**
     * 새 아이디 여부 판단
     *
     * @param username 회원 아이디
     * @return 새 아이디 여부
     */
    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);
        return dupCount <= 0;
    }

    /**
     * 새 닉네임 여부 판단
     *
     * @param nickname 닉네임
     * @return 새 닉네임 여부
     */
    private boolean isNewNickname(String nickname) {
        long dupCount = memberRepository.countByNickname(nickname);
        return dupCount <= 0;
    }

    /**
     * 회원 리스트 조회
     *
     * @param pageNum 페이지 번호
     * @param username 회원 아이디
     * @param memberGroup 회원 권한
     * @return 회원 리스트
     */
    public ListResult<MemberAdminListItem> getMembers(
            int pageNum,
            String username,
            MemberGroup memberGroup
    ) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);

        Page<Member> originList = getData(pageRequest, username, memberGroup);
        List<MemberAdminListItem> result = new LinkedList<>();

        originList.getContent().forEach(member -> {
            MemberAdminListItem memberAdminListItem = new MemberAdminListItem.MemberAdminListItemBuilder(member).build();
            result.add(memberAdminListItem);
        });

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    /**
     * 페이징 처리하여 원본 회원 리스트 호출
     *
     * @param pageable 페이징 관련 정보
     * @param username 회원 아이디
     * @param memberGroup 회원 권한
     * @return 원본 회원 리스트
     */
    private Page<Member> getData(Pageable pageable, String username, MemberGroup memberGroup) {
        // Criteria 라는 단어 자체가 기준, 조건 이라는 뜻임.
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder(); // 조건을 넣을 수 있게 해주는 빌더를 가져옴.
        CriteriaQuery<Member> criteriaQuery = criteriaBuilder.createQuery(Member.class); // 기본 쿼리 생성(member select)

        // ※ 실제로는 여기에 기재된 엔티티들만 잘 설정하면 됨. ※
        Root<Member> root = criteriaQuery.from(Member.class); // 대장 엔티티를 설정해 줌.

        List<Predicate> predicates = new LinkedList<>(); // 검색 조건들을 넣을 리스트를 생성함.
        // ※ 실제로는 여기에 기재된 검색 조건들만 잘 기재하면 됨. ※
        // StringUtils.length 를 쓰는 이유. (build.gradle에 새로운 패키지 추가해놨음.)
        // String에 글자수를 가져와라 라고 length 를 쓰게 되면 null값의 경우 에러를 내뱉음. (nullpoint exception)
        // 하지만 common-lang3 패키지의 StringUtils.length 를 쓰게 되면 null값을 넣어도 에러를 뱉지 않고 숫자 0을 줌.

        // 만약 검색필터에서 아이디 검색하기를 원한다면(글자수가 0글자 초과일 경우) root entity (위에서 member라고 지정했음)의 username 필드에서 해당 단어를 포함하는(like) 조건을 추가함.
        if (StringUtils.length(username) > 0) predicates.add(criteriaBuilder.like(root.get("username"), "%" + username + "%")); // like 조건 추가

        // 영어단어를 잘 읽고 아래 조건들은 말로 해석하면 됨.
        if (memberGroup != null) predicates.add(criteriaBuilder.equal(root.get("memberGroup"), memberGroup));
        predicates.add(criteriaBuilder.equal(root.get("isEnabled"), true));
        // ※ 실제로는 여기까지 기재된 검색 조건들만 잘 기재하면 됨. ※

        Predicate[] predArray = new Predicate[predicates.size()]; // 총 where 조건의 갯수를 구함.
        predicates.toArray(predArray); // 리스트를 배열로 변환
        criteriaQuery.where(predArray); // 기본쿼리에 where 문을 생성해서 붙임.

        TypedQuery<Member> query = entityManager.createQuery(criteriaQuery); // 데이터베이스에 쿼리를 날림.

        int totalRows = query.getResultList().size(); // 총 데이터 개수를 가져옴.

        // 사용자한테 제공 될 결과데이터들을 가져옴.
        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize()); // 데이터를 가져오는데 시작지점
        query.setMaxResults(pageable.getPageSize()); // 데이터를 가져오는데 종료지점

        return new PageImpl<>(query.getResultList(), pageable, totalRows); // 페이징을 구현한 데이터를 반환함. (현재 선택된 페이지의 데이터들, 페이징 객체, 총 데이터 수)
    }

    /**
     * 회원 비밀번호 변경
     *
     * @param member 회원 정보
     * @param updateRequest 변경할 비밀번호 정보
     */
    public void putMemberPassword(Member member, MemberPasswordUpdateRequest updateRequest) {
        if (!passwordEncoder.matches(updateRequest.getPassword(), member.getPassword())) throw new CPasswordNotEqualReException();
        if (updateRequest.getPassword().equals(updateRequest.getPasswordUpdate())) throw new CPasswordNewDuplicationException();
        if (!updateRequest.getPasswordUpdate().equals(updateRequest.getPasswordUpdateRe())) throw new CPasswordNewNotEqualException();

        updateRequest.setPasswordUpdate(passwordEncoder.encode(updateRequest.getPasswordUpdate()));

        member.putPassword(updateRequest.getPasswordUpdate());
        memberRepository.save(member);
    }

    /**
     * 회원 정보 수정
     *
     * @param member 회원 정보
     * @param updateRequest 변경할 회원 정보
     */
    public void putMemberData(Member member, MemberDataUpdateRequest updateRequest) {
        if (!isNewNickname(updateRequest.getNickname())) throw new CNicknameDuplicationException();

        member.putMemberData(updateRequest);
        memberRepository.save(member);
    }

    /**
     * 내 계정 탈퇴
     *
     * @param member 회원 정보
     */
    public void delMyAccount(Member member) {
        if (member.getMemberGroup().equals(MemberGroup.ROLE_ADMIN)) throw new CAdminDeleteFailedException();
        if (!member.getIsEnabled()) throw new CUserNotExistException();
        member.putIsEnabled();
        memberRepository.save(member);
    }

    /**
     * 회원 삭제
     *
     * @param memberId 회원 시퀀스
     */
    public void delMember(long memberId) {
        Member member = memberRepository.findById(memberId).orElseThrow(CUserNotExistException::new);

        if (member.getMemberGroup().equals(MemberGroup.ROLE_ADMIN)) throw new CAdminDeleteFailedException();
        if (!member.getIsEnabled()) throw new CUserNotExistException();

        member.putIsEnabled();
        memberRepository.save(member);
    }
}
