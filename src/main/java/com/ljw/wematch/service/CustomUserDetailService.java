package com.ljw.wematch.service;

import com.ljw.wematch.exception.CUsernameSignInFailedException;
import com.ljw.wematch.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private final MemberRepository memberRepository;

    /**
     * 인증을 위한 계정 정보 조회
     *
     * @param username 회원 아이디
     * @return 계정 정보
     * @throws UsernameNotFoundException 회원 아이디를 찾을 수 없다는 예외
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return memberRepository.findByUsername(username).orElseThrow(CUsernameSignInFailedException::new);
    }
}
