package com.ljw.wematch.model;

import com.ljw.wematch.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StatisticsResponse {
    @ApiModelProperty(notes = "오늘 가입자 수")
    private Long countNowMemberNew;

    @ApiModelProperty(notes = "오늘 건의사항 수")
    private Long countNowComplaintNew;

    @ApiModelProperty(notes = "오늘 경기 게시글 수")
    private Long countNowMatchNew;

    private StatisticsResponse(StatisticsResponseBuilder builder) {
        this.countNowMemberNew = builder.countNowMemberNew;
        this.countNowComplaintNew = builder.countNowComplaintNew;
        this.countNowMatchNew = builder.countNowMatchNew;
    }

    public static class StatisticsResponseBuilder implements CommonModelBuilder<StatisticsResponse> {
        private final Long countNowMemberNew;
        private final Long countNowComplaintNew;
        private final Long countNowMatchNew;

        public StatisticsResponseBuilder(long countNowMemberNew, long countNowComplaintNew, long countNowMatchNew) {
            this.countNowMemberNew = countNowMemberNew;
            this.countNowComplaintNew = countNowComplaintNew;
            this.countNowMatchNew = countNowMatchNew;
        }

        @Override
        public StatisticsResponse build() {
            return new StatisticsResponse(this);
        }
    }
}
