package com.ljw.wematch.model.complaintanswer;

import com.ljw.wematch.entity.ComplaintAnswer;
import com.ljw.wematch.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ComplaintAnswerItem {
    @ApiModelProperty(notes = "건의사항 답변 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "건의사항 시퀀스")
    private Long complaintId;

    @ApiModelProperty(notes = "회원 시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "회원 권한 이름")
    private String memberGroupName;

    @ApiModelProperty(notes = "글 내용")
    private String contents;

    private ComplaintAnswerItem(ComplaintAnswerItemBuilder builder) {
        this.id = builder.id;
        this.complaintId = builder.complaintId;
        this.memberId = builder.memberId;
        this.memberGroupName = builder.memberGroupName;
        this.contents = builder.contents;
    }

    public static class ComplaintAnswerItemBuilder implements CommonModelBuilder<ComplaintAnswerItem> {
        private final Long id;
        private final Long complaintId;
        private final Long memberId;
        private final String memberGroupName;
        private final String contents;

        public ComplaintAnswerItemBuilder(ComplaintAnswer complaintAnswer) {
            this.id = complaintAnswer.getId();
            this.complaintId = complaintAnswer.getComplaint().getId();
            this.memberId = complaintAnswer.getMember().getId();
            this.memberGroupName = complaintAnswer.getMember().getMemberGroup().getName();
            this.contents = complaintAnswer.getContents();
        }

        @Override
        public ComplaintAnswerItem build() {
            return new ComplaintAnswerItem(this);
        }
    }
}
