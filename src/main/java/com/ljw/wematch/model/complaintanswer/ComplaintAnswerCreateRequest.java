package com.ljw.wematch.model.complaintanswer;

import com.ljw.wematch.enums.ProcessingState;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ComplaintAnswerCreateRequest {
    @NotNull
    @ApiModelProperty(notes = "글 내용")
    private String contents;

    @NotNull
    @ApiModelProperty(notes = "건의사항 처리상태")
    private ProcessingState processingState;
}
