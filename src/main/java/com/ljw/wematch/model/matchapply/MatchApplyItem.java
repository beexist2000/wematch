package com.ljw.wematch.model.matchapply;

import com.ljw.wematch.entity.MatchApply;
import com.ljw.wematch.interfaces.CommonModelBuilder;
import com.ljw.wematch.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class MatchApplyItem {
    @ApiModelProperty(notes = "경기신청 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "수락 상태")
    private String acceptState;

    @ApiModelProperty(notes = "수락 상태 한글명")
    private String acceptStateName;

    @ApiModelProperty(notes = "등록 날짜")
    private String dateCreate;

    @ApiModelProperty(notes = "회원 시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "회원 닉네임")
    private String memberNickname;

    @ApiModelProperty(notes = "경기 시퀀스")
    private Long matchId;

    @ApiModelProperty(notes = "경기 제목")
    private String matchTitle;

    private MatchApplyItem(MatchApplyItemBuilder builder) {
        this.id = builder.id;
        this.acceptState = builder.acceptState;
        this.acceptStateName = builder.acceptStateName;
        this.dateCreate = builder.dateCreate;
        this.memberId = builder.memberId;
        this.memberNickname = builder.memberNickname;
        this.matchId = builder.matchId;
        this.matchTitle = builder.matchTitle;
    }

    public static class MatchApplyItemBuilder implements CommonModelBuilder<MatchApplyItem> {
        private final Long id;
        private final String acceptState;
        private final String acceptStateName;
        private final String dateCreate;
        private final Long memberId;
        private final String memberNickname;
        private final Long matchId;
        private final String matchTitle;

        public MatchApplyItemBuilder(MatchApply matchApply) {
            this.id = matchApply.getId();
            this.acceptState = matchApply.getAcceptState().toString();
            this.acceptStateName = matchApply.getAcceptState().getName();
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(matchApply.getDateCreate());
            this.memberId = matchApply.getMember().getId();
            this.memberNickname = matchApply.getMember().getNickname();
            this.matchId = matchApply.getMatch().getId();
            this.matchTitle = matchApply.getMatch().getTitle();
        }

        @Override
        public MatchApplyItem build() {
            return new MatchApplyItem(this);
        }
    }
}
