package com.ljw.wematch.model.matchapply;

import com.ljw.wematch.enums.AcceptState;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MatchApplyRequest {
    @NotNull
    @ApiModelProperty(notes = "신청 상태")
    private AcceptState acceptState;
}
