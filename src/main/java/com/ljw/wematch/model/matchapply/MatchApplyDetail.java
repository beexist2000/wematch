package com.ljw.wematch.model.matchapply;

import com.ljw.wematch.entity.MatchApply;
import com.ljw.wematch.interfaces.CommonModelBuilder;
import com.ljw.wematch.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MatchApplyDetail {
    @ApiModelProperty(notes = "경기신청 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "수락 상태")
    private String acceptState;

    @ApiModelProperty(notes = "수락 상태 한글명")
    private String acceptStateName;

    @ApiModelProperty(notes = "등록 날짜")
    private String dateCreate;

    @ApiModelProperty(notes = "등록 날짜")
    private String dateAccept;

    @ApiModelProperty(notes = "거절 날짜")
    private String dateRefuse;

    @ApiModelProperty(notes = "회원 시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "회원 닉네임")
    private String memberNickname;

    @ApiModelProperty(notes = "회원 연락처")
    private String memberPhone;

    @ApiModelProperty(notes = "경기 시퀀스")
    private Long matchId;

    @ApiModelProperty(notes = "경기 제목")
    private String matchTitle;

    @ApiModelProperty(notes = "경기일")
    private String sportDate;

    private MatchApplyDetail(MatchApplyDetailBuilder builder) {
        this.id = builder.id;
        this.acceptState = builder.acceptState;
        this.acceptStateName = builder.acceptStateName;
        this.dateCreate = builder.dateCreate;
        this.dateAccept = builder.dateAccept;
        this.dateRefuse = builder.dateRefuse;
        this.memberId = builder.memberId;
        this.memberNickname = builder.memberNickname;
        this.memberPhone = builder.memberPhone;
        this.matchId = builder.matchId;
        this.matchTitle = builder.matchTitle;
        this.sportDate = builder.sportDate;
    }

    public static class MatchApplyDetailBuilder implements CommonModelBuilder<MatchApplyDetail> {
        private final Long id;
        private final String acceptState;
        private final String acceptStateName;
        private final String dateCreate;
        private final String dateAccept;
        private final String dateRefuse;
        private final Long memberId;
        private final String memberNickname;
        private final String memberPhone;
        private final Long matchId;
        private final String matchTitle;
        private final String sportDate;

        public MatchApplyDetailBuilder(MatchApply matchApply) {
            this.id = matchApply.getId();
            this.acceptState = matchApply.getAcceptState().toString();
            this.acceptStateName = matchApply.getAcceptState().getName();
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(matchApply.getDateCreate());
            this.dateAccept = matchApply.getDateAccept() == null ? "-" : CommonFormat.convertLocalDateTimeToString(matchApply.getDateAccept());
            this.dateRefuse = matchApply.getDateRefuse() == null ? "-" : CommonFormat.convertLocalDateTimeToString(matchApply.getDateRefuse());
            this.memberId = matchApply.getMember().getId();
            this.memberNickname = matchApply.getMember().getNickname();
            this.memberPhone = matchApply.getMember().getPhone();
            this.matchId = matchApply.getMatch().getId();
            this.matchTitle = matchApply.getMatch().getTitle();
            this.sportDate = CommonFormat.convertLocalDateToString(matchApply.getMatch().getSportDate());
        }

        @Override
        public MatchApplyDetail build() {
            return new MatchApplyDetail(this);
        }
    }
}
