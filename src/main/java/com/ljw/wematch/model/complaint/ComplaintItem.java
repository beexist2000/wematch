package com.ljw.wematch.model.complaint;

import com.ljw.wematch.entity.Complaint;
import com.ljw.wematch.interfaces.CommonModelBuilder;
import com.ljw.wematch.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ComplaintItem {
    @ApiModelProperty(notes = "건의사항 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "글 제목")
    private String title;

    @ApiModelProperty(notes = "처리 상태")
    private String processingState;

    @ApiModelProperty(notes = "처리 상태 한글명")
    private String processingStateName;

    @ApiModelProperty(notes = "등록 날짜")
    private String dateCreate;

    @ApiModelProperty(notes = "회원 시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "회원 아이디")
    private String memberUsername;

    private ComplaintItem(ComplaintItemBuilder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.processingState = builder.processingState;
        this.processingStateName = builder.processingStateName;
        this.dateCreate = builder.dateCreate;
        this.memberId = builder.memberId;
        this.memberUsername = builder.memberUsername;
    }

    public static class ComplaintItemBuilder implements CommonModelBuilder<ComplaintItem> {
        private final Long id;
        private final String title;
        private final String processingState;
        private final String processingStateName;
        private final String dateCreate;
        private final Long memberId;
        private final String memberUsername;

        public ComplaintItemBuilder(Complaint complaint) {
            this.id = complaint.getId();
            this.title = complaint.getTitle();
            this.processingState = complaint.getProcessingState().toString();
            this.processingStateName = complaint.getProcessingState().getName();
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(complaint.getDateCreate());
            this.memberId = complaint.getMember().getId();
            this.memberUsername = complaint.getMember().getUsername();
        }

        @Override
        public ComplaintItem build() {
            return new ComplaintItem(this);
        }
    }
}
