package com.ljw.wematch.model.complaint;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ComplaintCreateRequest {
    @NotNull
    @Length(min = 2, max = 30)
    @ApiModelProperty(notes = "글 제목")
    private String title;

    @NotNull
    @ApiModelProperty(notes = "글 내용")
    private String contents;
}
