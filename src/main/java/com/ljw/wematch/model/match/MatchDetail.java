package com.ljw.wematch.model.match;

import com.ljw.wematch.entity.Match;
import com.ljw.wematch.enums.BooleanFilter;
import com.ljw.wematch.interfaces.CommonModelBuilder;
import com.ljw.wematch.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MatchDetail {
    @ApiModelProperty(notes = "경기 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "경기 주소")
    private String addressName;

    @ApiModelProperty(notes = "경기 장소")
    private String placeName;

    @ApiModelProperty(notes = "경기일")
    private String sportDate;

    @ApiModelProperty(notes = "글 제목")
    private String title;

    @ApiModelProperty(notes = "글 내용")
    private String contents;

    @ApiModelProperty(notes = "등록날짜")
    private String dateCreate;

    @ApiModelProperty(notes = "회원 시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "회원 닉네임")
    private String memberNickname;

    @ApiModelProperty(notes = "글쓴이 확인 여부")
    private BooleanFilter isMine;

    private MatchDetail(MatchDetailBuilder builder) {
        this.id = builder.id;
        this.addressName = builder.addressName;
        this.placeName = builder.placeName;
        this.sportDate = builder.sportDate;
        this.title = builder.title;
        this.contents = builder.contents;
        this.dateCreate = builder.dateCreate;
        this.memberId = builder.memberId;
        this.memberNickname = builder.memberNickname;
        this.isMine = builder.isMine;
    }

    public static class MatchDetailBuilder implements CommonModelBuilder<MatchDetail> {
        private final Long id;
        private final String addressName;
        private final String placeName;
        private final String sportDate;
        private final String title;
        private final String contents;
        private final String dateCreate;
        private final Long memberId;
        private final String memberNickname;
        private final BooleanFilter isMine;


        public MatchDetailBuilder(Match match, boolean isMine) {
            this.id = match.getId();
            this.addressName = match.getAddressName();
            this.placeName = match.getPlaceName();
            this.sportDate = CommonFormat.convertLocalDateToString(match.getSportDate());
            this.title = match.getTitle();
            this.contents = match.getContents();
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(match.getDateCreate());
            this.memberId = match.getMember().getId();
            this.memberNickname = match.getMember().getIsEnabled() ? match.getMember().getNickname() : "탈퇴한 회원";
            this.isMine = isMine ? BooleanFilter.Y : BooleanFilter.N;
        }

        @Override
        public MatchDetail build() {
            return new MatchDetail(this);
        }
    }
}
