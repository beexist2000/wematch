package com.ljw.wematch.model.match;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MatchRequest {
    @NotNull
    @Length(min = 2, max = 30)
    @ApiModelProperty(notes = "경기 장소(2~30자)")
    private String placeName;

    @NotNull
    @Length(min = 2, max = 40)
    @ApiModelProperty(notes = "경기 주소(2~40자)")
    private String addressName;

    @NotNull
    @Length(min = 2, max = 30)
    @ApiModelProperty(notes = "글 제목(2~30자)")
    private String title;

    @NotNull
    @ApiModelProperty(notes = "글 내용")
    private String contents;

    @NotNull
    @ApiModelProperty(notes = "경기일")
    private LocalDate sportDate;

    @NotNull
    @ApiModelProperty(notes = "경도")
    private Double posX;

    @NotNull
    @ApiModelProperty(notes = "위도")
    private Double posY;
}
