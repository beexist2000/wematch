package com.ljw.wematch.model.match;

import com.ljw.wematch.entity.Match;
import com.ljw.wematch.interfaces.CommonModelBuilder;
import com.ljw.wematch.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MatchItem {
    @ApiModelProperty(notes = "경기 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "경기 주소")
    private String addressName;

    @ApiModelProperty(notes = "글 제목")
    private String title;

    @ApiModelProperty(notes = "수락받은 인원")
    private Long personCountAccept;

    @ApiModelProperty(notes = "신청대기 인원")
    private Long personCountStandby;

    @ApiModelProperty(notes = "등록날짜")
    private String dateCreate;

    @ApiModelProperty(notes = "회원 시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "회원 닉네임")
    private String memberNickname;

    private MatchItem(MatchItemBuilder builder) {
        this.id = builder.id;
        this.addressName = builder.addressName;
        this.title = builder.title;
        this.personCountAccept = builder.personCountAccept;
        this.personCountStandby = builder.personCountStandby;
        this.dateCreate = builder.dateCreate;
        this.memberId = builder.memberId;
        this.memberNickname = builder.memberNickname;
    }

    public static class MatchItemBuilder implements CommonModelBuilder<MatchItem> {
        private final Long id;
        private final String addressName;
        private final String title;
        private final Long personCountAccept;
        private final Long personCountStandby;
        private final String dateCreate;
        private final Long memberId;
        private final String memberNickname;

        public MatchItemBuilder(Match match, long personCountAccept, long personCountStandby) {
            this.id = match.getId();
            this.addressName = match.getAddressName();
            this.title = match.getTitle();
            this.personCountAccept = personCountAccept;
            this.personCountStandby = personCountStandby;
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(match.getDateCreate());
            this.memberId = match.getMember().getId();
            this.memberNickname = match.getMember().getNickname();
        }

        @Override
        public MatchItem build() {
            return new MatchItem(this);
        }
    }
}
