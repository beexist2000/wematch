package com.ljw.wematch.model.member;

import com.ljw.wematch.entity.Member;
import com.ljw.wematch.interfaces.CommonModelBuilder;
import com.ljw.wematch.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberAdminListItem {
    @ApiModelProperty(notes = "회원 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "사용자 권한")
    private String memberGroup;

    @ApiModelProperty(notes = "사용자 권한 한글명")
    private String memberGroupName;

    @ApiModelProperty(notes = "사용자 아이디")
    private String userFullName;

    @ApiModelProperty(notes = "등록시간")
    private String dateCreate;

    private MemberAdminListItem(MemberAdminListItemBuilder builder) {
        this.id = builder.id;
        this.memberGroup = builder.memberGroup;
        this.memberGroupName = builder.memberGroupName;
        this.userFullName = builder.userFullName;
        this.dateCreate = builder.dateCreate;
    }

    public static class MemberAdminListItemBuilder implements CommonModelBuilder<MemberAdminListItem> {
        private final Long id;
        private final String memberGroup;
        private final String memberGroupName;
        private final String userFullName;
        private final String dateCreate;


        public MemberAdminListItemBuilder(Member member) {
            this.id = member.getId();
            this.memberGroup = member.getMemberGroup().toString();
            this.memberGroupName = member.getMemberGroup().getName();
            this.userFullName = member.getUsername() + "(" + member.getNickname() + ")";
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(member.getDateCreate());
        }

        @Override
        public MemberAdminListItem build() {
            return new MemberAdminListItem(this);
        }
    }
}
