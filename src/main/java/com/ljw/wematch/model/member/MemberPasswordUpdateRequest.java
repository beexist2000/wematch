package com.ljw.wematch.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberPasswordUpdateRequest {
    @NotNull
    @Length(min = 8, max = 20)
    @ApiModelProperty(notes = "비밀번호(8~20자)")
    private String password;

    @NotNull
    @Length(min = 8, max = 20)
    @ApiModelProperty(notes = "새 비밀번호(8~20자)")
    private String passwordUpdate;

    @NotNull
    @Length(min = 8, max = 20)
    @ApiModelProperty(notes = "새 비밀번호 확인(8~20자)")
    private String passwordUpdateRe;
}
