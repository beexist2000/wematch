package com.ljw.wematch.model.member;

import com.ljw.wematch.enums.Gender;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberCreateRequest {
    @NotNull
    @Length(min = 2, max = 30)
    @ApiModelProperty(notes = "사용자 아이디(2~30자)")
    private String username;

    @NotNull
    @Length(min = 8, max = 20)
    @ApiModelProperty(notes = "사용자 비밀번호(8~20자)")
    private String password;

    @NotNull
    @Length(min = 8, max = 20)
    @ApiModelProperty(notes = "사용자 비밀번호 확인(8~20자)")
    private String passwordRe;

    @NotNull
    @Length(min = 2, max = 10)
    @ApiModelProperty(notes = "닉네임(2~10자)")
    private String nickname;

    @NotNull
    @Length(min = 2, max = 20)
    @ApiModelProperty(notes = "사용자 이름(2~20자)")
    private String name;

    @NotNull
    @ApiModelProperty(notes = "생일")
    private LocalDate birthday;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    @ApiModelProperty(notes = "성별")
    private Gender gender;

    @NotNull
    @Length(min = 13, max = 13)
    @ApiModelProperty(notes = "연락처")
    private String phone;
}
