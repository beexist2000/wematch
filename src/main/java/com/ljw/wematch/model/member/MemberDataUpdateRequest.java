package com.ljw.wematch.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberDataUpdateRequest {
    @NotNull
    @Length(min = 2, max = 10)
    @ApiModelProperty(notes = "닉네임")
    private String nickname;

    @NotNull
    @Length(min = 13, max = 13)
    @ApiModelProperty(notes = "연락처")
    private String phone;

    @NotNull
    @Length(min = 2, max = 15)
    @ApiModelProperty(notes = "주종목")
    private String sportType;

    @NotNull
    @Length(min = 2, max = 40)
    @ApiModelProperty(notes = "활동 지역")
    private String location;
}
