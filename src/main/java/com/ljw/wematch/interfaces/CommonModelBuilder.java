package com.ljw.wematch.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
