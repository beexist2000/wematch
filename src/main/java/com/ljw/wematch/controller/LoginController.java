package com.ljw.wematch.controller;

import com.ljw.wematch.enums.MemberGroup;
import com.ljw.wematch.model.common.SingleResult;
import com.ljw.wematch.model.member.LoginRequest;
import com.ljw.wematch.model.member.LoginResponse;
import com.ljw.wematch.service.LoginService;
import com.ljw.wematch.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "로그인")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member/login")
public class LoginController {
    private final LoginService loginService;

    @ApiOperation(value = "웹 - 관리자 로그인")
    @PostMapping("/web/admin")
    public SingleResult<LoginResponse> doLoginAdmin(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_ADMIN, loginRequest, "WEB"));
    }

    @ApiOperation(value = "웹 - 부관리자 로그인")
    @PostMapping("/web/sub-admin")
    public SingleResult<LoginResponse> doLoginSubAdmin(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_SUB_ADMIN, loginRequest, "WEB"));
    }

    @ApiOperation(value = "앱 - 일반유저 로그인")
    @PostMapping("/app/user")
    public SingleResult<LoginResponse> doLoginUser(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_USER, loginRequest, "APP"));
    }
}
