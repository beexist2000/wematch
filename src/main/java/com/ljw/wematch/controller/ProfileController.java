package com.ljw.wematch.controller;

import com.ljw.wematch.model.common.SingleResult;
import com.ljw.wematch.model.member.ProfileResponse;
import com.ljw.wematch.service.ProfileService;
import com.ljw.wematch.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "프로필 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member/profile")
public class ProfileController {
    private final ProfileService profileService;

    @ApiOperation(value = "내 프로필 조회")
    @GetMapping("/my")
    public SingleResult<ProfileResponse> getMyProfile() {
        return ResponseService.getSingleResult(profileService.getProfile());
    }
}
