package com.ljw.wematch.controller;

import com.ljw.wematch.model.StatisticsResponse;
import com.ljw.wematch.model.common.SingleResult;
import com.ljw.wematch.service.ResponseService;
import com.ljw.wematch.service.StatisticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "통계 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/statistics")
public class StatisticsController {
    private final StatisticsService statisticsService;

    @ApiOperation(value = "오늘 통계")
    @GetMapping("/now")
    public SingleResult<StatisticsResponse> getNowStatistics() {
        return ResponseService.getSingleResult(statisticsService.getNowStatistics());
    }
}
