package com.ljw.wematch.controller;

import com.ljw.wematch.entity.Member;
import com.ljw.wematch.model.common.CommonResult;
import com.ljw.wematch.model.common.ListResult;
import com.ljw.wematch.model.common.SingleResult;
import com.ljw.wematch.model.complaint.ComplaintCreateRequest;
import com.ljw.wematch.model.complaint.ComplaintDetail;
import com.ljw.wematch.model.complaint.ComplaintItem;
import com.ljw.wematch.model.complaintanswer.ComplaintAnswerCreateRequest;
import com.ljw.wematch.model.complaintanswer.ComplaintAnswerItem;
import com.ljw.wematch.service.ComplaintService;
import com.ljw.wematch.service.ProfileService;
import com.ljw.wematch.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "건의사항 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/complain")
public class ComplaintController {
    private final ProfileService profileService;
    private final ComplaintService complaintService;

    @ApiOperation(value = "건의사항 등록")
    @PostMapping("/new")
    public CommonResult setComplaint(@RequestBody @Valid ComplaintCreateRequest createRequest) {
        Member member = profileService.getMemberData();
        complaintService.setComplaint(member, createRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "건의사항답변 등록")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "complainId", value = "건의사항 시퀀스", required = true)
    })
    @PostMapping("/answer/complain-id/{complainId}")
    public CommonResult setComplainAnswer(@PathVariable long complainId, @RequestBody @Valid ComplaintAnswerCreateRequest createRequest) {
        Member member = profileService.getMemberData();
        complaintService.setComplaintAnswer(member, complainId, createRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "내 건의사항 리스트 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "페이지 번호", required = true),
            @ApiImplicitParam(name = "year", value = "기준년도", required = true),
            @ApiImplicitParam(name = "month", value = "기준월", required = true)
    })
    @GetMapping("/list/my/page/{pageNum}")
    public ListResult<ComplaintItem> getMyComplaints(
            @PathVariable int pageNum,
            @RequestParam(value = "year") int year,
            @RequestParam(value = "month") int month
    ) {
        Member member = profileService.getMemberData();

        return ResponseService.getListResult(complaintService.getMyComplaints(member, pageNum, year, month), true);
    }

    @ApiOperation(value = "건의사항 리스트 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "페이지 번호", required = true),
            @ApiImplicitParam(name = "year", value = "기준년도", required = true),
            @ApiImplicitParam(name = "month", value = "기준월", required = true)
    })
    @GetMapping("/list/page/{pageNum}")
    public ListResult<ComplaintItem> getComplaints(
            @PathVariable int pageNum,
            @RequestParam(value = "year") int year,
            @RequestParam(value = "month") int month
    ) {
        return ResponseService.getListResult(complaintService.getComplaint(pageNum, year, month), true);
    }

    @ApiOperation(value = "건의사항 세부정보 불러오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "complaintId", value = "건의사항 시퀀스", required = true)
    })
    @GetMapping("/detail/complaint-id/{complaintId}")
    public SingleResult<ComplaintDetail> getComplaintDetail(@PathVariable int complaintId) {
        return ResponseService.getSingleResult(complaintService.getComplaintDetail(complaintId));
    }

    @ApiOperation(value = "건의사항 답글 리스트 불러오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "complaintId", value = "건의사항 시퀀스", required = true)
    })
    @GetMapping("/answer/list/complaint-id/{complaintId}")
    public ListResult<ComplaintAnswerItem> getComplaintAnswers(@PathVariable int complaintId) {
        return ResponseService.getListResult(complaintService.getComplaintAnswers(complaintId), true);
    }
}
