package com.ljw.wematch.controller;

import com.ljw.wematch.entity.Member;
import com.ljw.wematch.model.common.CommonResult;
import com.ljw.wematch.model.common.ListResult;
import com.ljw.wematch.model.common.SingleResult;
import com.ljw.wematch.model.match.MatchDetail;
import com.ljw.wematch.model.match.MatchItem;
import com.ljw.wematch.model.match.MatchRequest;
import com.ljw.wematch.model.matchapply.MatchApplyDetail;
import com.ljw.wematch.model.matchapply.MatchApplyItem;
import com.ljw.wematch.model.matchapply.MatchApplyRequest;
import com.ljw.wematch.service.MatchService;
import com.ljw.wematch.service.ProfileService;
import com.ljw.wematch.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "경기 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/match")
public class MatchController {
    private final ProfileService profileService;
    private final MatchService matchService;

    @ApiOperation(value = "경기 등록")
    @PostMapping("/new")
    public CommonResult setMatch(@RequestBody @Valid MatchRequest request) {
        Member member = profileService.getMemberData();
        matchService.setMatch(member, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "내 경기 리스트 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "페이지 번호", required = true),
            @ApiImplicitParam(name = "year", value = "등록일 기준년도", required = true),
            @ApiImplicitParam(name = "month", value = "등록일 기준월", required = true)
    })
    @GetMapping("/my/list/page/{pageNum}")
    public ListResult<MatchItem> getMyMatches(
            @PathVariable int pageNum,
            @RequestParam(value = "year") int year,
            @RequestParam(value = "month") int month
    ) {
        Member member = profileService.getMemberData();

        return ResponseService.getListResult(matchService.getMyMatches(member, pageNum, year, month), true);
    }

    @ApiOperation(value = "경기 리스트 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "페이지 번호", required = true),
            @ApiImplicitParam(name = "addressName", value = "경기 주소"),
            @ApiImplicitParam(name = "title", value = "글 제목"),
            @ApiImplicitParam(name = "dateStart", value = "시작일", required = true),
            @ApiImplicitParam(name = "dateEnd", value = "종료일", required = true)
    })
    @GetMapping("/list/page/{pageNum}")
    public ListResult<MatchItem> getMatches(
            @PathVariable int pageNum,
            @RequestParam(value = "addressName", required = false) String addressName,
            @RequestParam(value = "title", required = false) String title,
            @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "dateStart") LocalDate dateStart,
            @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "dateEnd") LocalDate dateEnd
    ) {
        return ResponseService.getListResult(matchService.getMatches(addressName, title, pageNum, dateStart, dateEnd), true);
    }

    @ApiOperation(value = "경기 상세정보 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "matchId", value = "경기 시퀀스", required = true)
    })
    @GetMapping("/match-id/{matchId}")
    public SingleResult<MatchDetail> getMatch(@PathVariable long matchId) {
        Member member = profileService.getMemberData();

        return ResponseService.getSingleResult(matchService.getMatch(member, matchId));
    }

    @ApiOperation(value = "경기 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "matchId", value = "경기 시퀀스", required = true)
    })
    @PutMapping("/match-id/{matchId}")
    public CommonResult putMatch(@PathVariable long matchId, @RequestBody @Valid MatchRequest request) {
        matchService.putMatch(matchId, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "경기 참가신청")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "matchId", value = "경기 시퀀스", required = true)
    })
    @PostMapping("/apply/match-id/{matchId}")
    public CommonResult setMatchApply(@PathVariable long matchId) {
        Member member = profileService.getMemberData();
        matchService.setMatchApply(member, matchId);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "신청 처리")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "matchApplyId", value = "신청 시퀀스", required = true)
    })
    @PutMapping("/apply/match-apply-id/{matchApplyId}")
    public CommonResult putMatchApplyState(@PathVariable long matchApplyId, @RequestBody @Valid MatchApplyRequest request) {
        matchService.putMatchApplyState(matchApplyId, request.getAcceptState());

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "내 경기신청 리스트 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "페이지 번호", required = true),
            @ApiImplicitParam(name = "year", value = "등록일 기준년도", required = true),
            @ApiImplicitParam(name = "month", value = "등록일 기준월", required = true)
    })
    @GetMapping("/apply/list/page/{pageNum}")
    public ListResult<MatchApplyItem> getMyMatchApplies(
            @PathVariable int pageNum,
            @RequestParam(value = "year") int year,
            @RequestParam(value = "month") int month
    ) {
        Member member = profileService.getMemberData();

        return ResponseService.getListResult(matchService.getMyMatchApplies(member, pageNum, year, month), true);
    }

    @ApiOperation(value = "경기신청 리스트 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "matchId", value = "경기 시퀀스", required = true),
            @ApiImplicitParam(name = "pageNum", value = "페이지 번호", required = true)
    })
    @GetMapping("/apply/list/page/{pageNum}/match-id/{matchId}")
    public ListResult<MatchApplyItem> getMatchApplies(@PathVariable long matchId, @PathVariable int pageNum) {
        return ResponseService.getListResult(matchService.getMatchApplies(matchId, pageNum), true);
    }

    @ApiOperation(value = "경기신청 상세정보 불러오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "matchApplyId", value = "경기 신청 시퀀스", required = true)
    })
    @GetMapping("/apply/match-apply-id/{matchApplyId}")
    public SingleResult<MatchApplyDetail> getMatchApply(@PathVariable long matchApplyId) {
        return ResponseService.getSingleResult(matchService.getMatchApply(matchApplyId));
    }

    @ApiOperation(value = "경기 취소")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "matchId", value = "경기 시퀀스", required = true)
    })
    @DeleteMapping("/match-id/{matchId}")
    public CommonResult delMatch(@PathVariable long matchId) {
        matchService.delMatch(matchId);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "신청 취소")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "matchApplyId", value = "신청 시퀀스", required = true)
    })
    @DeleteMapping("/apply/match-apply-id/{matchApplyId}")
    public CommonResult delMatchApply(@PathVariable long matchApplyId) {
        matchService.delMatchApply(matchApplyId);

        return ResponseService.getSuccessResult();
    }
}
