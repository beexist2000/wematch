package com.ljw.wematch.controller;

import com.ljw.wematch.entity.Member;
import com.ljw.wematch.enums.MemberGroup;
import com.ljw.wematch.model.common.CommonResult;
import com.ljw.wematch.model.common.ListResult;
import com.ljw.wematch.model.member.MemberAdminListItem;
import com.ljw.wematch.model.member.MemberCreateRequest;
import com.ljw.wematch.model.member.MemberDataUpdateRequest;
import com.ljw.wematch.model.member.MemberPasswordUpdateRequest;
import com.ljw.wematch.service.MemberDataService;
import com.ljw.wematch.service.ProfileService;
import com.ljw.wematch.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "회원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberDataService memberDataService;
    private final ProfileService profileService;

    @ApiOperation(value = "일반회원 생성")
    @PostMapping("/user")
    public CommonResult setUser(@RequestBody @Valid MemberCreateRequest createRequest) {
        memberDataService.setMember(MemberGroup.ROLE_USER, createRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "관리자 생성")
    @PostMapping("/admin")
    public CommonResult setAdmin(@RequestBody @Valid MemberCreateRequest createRequest) {
        memberDataService.setMember(MemberGroup.ROLE_SUB_ADMIN, createRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 리스트 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "페이지 번호", required = true),
            @ApiImplicitParam(name = "username", value = "회원 아이디"),
            @ApiImplicitParam(name = "memberGroup", value = "회원 권한")
    })
    @GetMapping("/search/page/{pageNum}")
    public ListResult<MemberAdminListItem> getMembers(
            @PathVariable int pageNum,
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "memberGroup", required = false) MemberGroup memberGroup
    ) {
        return ResponseService.getListResult(memberDataService.getMembers(pageNum, username, memberGroup), true);
    }

    @ApiOperation(value = "비밀번호 변경")
    @PutMapping("/new-password")
    public CommonResult putMemberPassword(@RequestBody @Valid MemberPasswordUpdateRequest updateRequest) {
        Member member = profileService.getMemberData();
        memberDataService.putMemberPassword(member, updateRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원정보 변경")
    @PutMapping("/data")
    public CommonResult putMemberData(@RequestBody @Valid MemberDataUpdateRequest updateRequest) {
        Member member = profileService.getMemberData();
        memberDataService.putMemberData(member, updateRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "내 계정 삭제")
    @DeleteMapping("/my-account")
    public CommonResult delMyAccount() {
        Member member = profileService.getMemberData();
        memberDataService.delMyAccount(member);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 삭제")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "회원 시퀀스", required = true)
    })
    @DeleteMapping("/member-id/{memberId}")
    public CommonResult delMember(@PathVariable long memberId) {
        memberDataService.delMember(memberId);
        return ResponseService.getSuccessResult();
    }
}
