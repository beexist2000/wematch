package com.ljw.wematch.lib;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class CommonFormat {
    public static String convertDoubleToPriceText(double value) {
        DecimalFormat format = new DecimalFormat("###,###");
        return format.format(value);
    }

    public static BigDecimal convertDoubleToDecimal(double value) {
        return BigDecimal.valueOf(value).setScale(0, RoundingMode.HALF_UP);
    }

    public static String convertLocalDateToString(LocalDate date) {
        if (date == null) return "-";
        String monthKey = date.getMonthValue() < 10 ? "0" + date.getMonthValue() : String.valueOf(date.getMonthValue());
        String dayKey = date.getDayOfMonth() < 10 ? "0" + date.getDayOfMonth() : String.valueOf(date.getDayOfMonth());
        return date.getYear() + "-" + monthKey + "-" + dayKey;
       // return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public static String convertLocalDateTimeToString(LocalDateTime dateTime) {
        if (dateTime == null) return "-";
        String monthKey = dateTime.getMonthValue() < 10 ? "0" + dateTime.getMonthValue() : String.valueOf(dateTime.getMonthValue());
        String dayKey = dateTime.getDayOfMonth() < 10 ? "0" + dateTime.getDayOfMonth() : String.valueOf(dateTime.getDayOfMonth());
        return dateTime.getYear() + "-" + monthKey + "-" + dayKey + " " + dateTime.getHour() + ":" + dateTime.getMinute();
        // todo: format error
//        return new SimpleDateFormat("yyyyMMdd").format(dateTime);
    }

    public static String convertLocalDateTimeToSimpleString(LocalDateTime dateTime) {
        String monthKey = dateTime.getMonthValue() < 10 ? "0" + dateTime.getMonthValue() : String.valueOf(dateTime.getMonthValue());
        String dayKey = dateTime.getDayOfMonth() < 10 ? "0" + dateTime.getDayOfMonth() : String.valueOf(dateTime.getDayOfMonth());
        String hourKey = dateTime.getHour() < 10 ? "0" + dateTime.getHour() : String.valueOf(dateTime.getHour());
        String minuteKey = dateTime.getMinute() < 10 ? "0" + dateTime.getMinute() : String.valueOf(dateTime.getMinute());
        String secondKey = dateTime.getSecond() < 10 ? "0" + dateTime.getSecond() : String.valueOf(dateTime.getSecond());
        return dateTime.getYear() + monthKey + dayKey + hourKey + minuteKey + secondKey ;
        // todo: format error
//        return new SimpleDateFormat("yyyyMMdd").format(dateTime);
    }

    public static String convertLocalTimeToString(LocalTime time) {
        if (time == null) return "-";
        return new SimpleDateFormat("HH:mm").format(time);
    }

}
