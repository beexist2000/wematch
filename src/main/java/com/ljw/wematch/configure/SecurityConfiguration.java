package com.ljw.wematch.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final JwtTokenProvider jwtTokenProvider;

    private static final String[] AUTH_WHITELIST = {
            "/swagger-ui.html",
            "/swagger-resources/**",
            "/webjars/**",
            "/v2/api-docs"
    };

    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        webSecurity.ignoring().antMatchers(AUTH_WHITELIST);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .authorizeRequests()
                        .antMatchers(HttpMethod.GET, "/exception/**").permitAll() // 전체 허용
                        .antMatchers("/v1/member/login/**").permitAll() // 전체허용
                        .antMatchers("/v1/auth-test/test-admin").hasAnyRole("ADMIN", "SUB_ADMIN")
                        .antMatchers("/v1/auth-test/test-user").hasAnyRole("USER")
                        .antMatchers("/v1/auth-test/test-all").hasAnyRole("ADMIN", "SUB_ADMIN", "USER")
                        .antMatchers("/v1/auth-test/login-all/**").hasAnyRole("ADMIN", "SUB_ADMIN" , "USER")
                        .antMatchers("/v1/member/new-password").hasAnyRole("ADMIN", "SUB_ADMIN", "USER")
                        .antMatchers("/v1/member/user").permitAll()
                        .antMatchers("/v1/member/data").hasAnyRole("ADMIN", "SUB_ADMIN", "USER")
                        .antMatchers(HttpMethod.GET,"/v1/member/search/page/**").hasAnyRole("ADMIN", "SUB_ADMIN")
                        .antMatchers("/v1/member/my-account").hasAnyRole("ADMIN", "SUB_ADMIN", "USER")
                        .antMatchers(HttpMethod.DELETE ,"/v1/member/member-id/**").hasAnyRole("ADMIN", "SUB_ADMIN")
                        .antMatchers("/v1/member/profile/my").hasAnyRole("ADMIN", "SUB_ADMIN", "USER")
                        .antMatchers("/v1/match/new").hasAnyRole("USER")
                        .antMatchers(HttpMethod.GET, "/v1/match/my/list/page/**").hasAnyRole("USER")
                        .antMatchers(HttpMethod.GET, "/v1/match/list/page/**").hasAnyRole("USER")
                        .antMatchers(HttpMethod.GET, "/v1/match/match-id/**").hasAnyRole("ADMIN", "SUB_ADMIN", "USER")
                        .antMatchers(HttpMethod.PUT, "/v1/match/match-id/**").hasAnyRole("USER")
                        .antMatchers(HttpMethod.DELETE, "/v1/match/match-id/**").hasAnyRole("USER")
                        .antMatchers(HttpMethod.POST, "/v1/match/apply/match-id/**").hasAnyRole("USER")
                        .antMatchers(HttpMethod.GET, "/v1/match/apply/list/page/**").hasAnyRole("USER")
                        .antMatchers(HttpMethod.GET, "/v1/match/apply/match-apply-id/**").hasAnyRole("USER")
                        .antMatchers(HttpMethod.PUT, "/v1/match/apply/match-apply-id/**").hasAnyRole("USER")
                        .antMatchers(HttpMethod.DELETE, "/v1/match/apply/match-apply-id/**").hasAnyRole("USER")
                        .antMatchers("/v1/complain/new").hasAnyRole("USER")
                        .antMatchers(HttpMethod.GET, "/v1/complain/list/my/page/**").hasAnyRole("USER")
                        .antMatchers(HttpMethod.GET, "/v1/complain/list/page/**").hasAnyRole("ADMIN", "SUB_ADMIN")
                        .antMatchers(HttpMethod.GET, "/v1/complain/detail/complaint-id/**").hasAnyRole("ADMIN", "SUB_ADMIN", "USER")
                        .antMatchers(HttpMethod.GET, "/v1/complain/answer/list/complaint-id/**").hasAnyRole("ADMIN", "SUB_ADMIN", "USER")
                        .anyRequest().hasRole("ADMIN") // 기본 접근 권한은 ROLE_ADMIN
                .and()
                    .exceptionHandling().accessDeniedHandler(new CustomAccessDeniedHandler())
                .and()
                    .exceptionHandling().authenticationEntryPoint(new CustomAuthenticationEntryPoint())
                .and()
                    .addFilterBefore(new JwtAuthenticationFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);

    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source =
                new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOriginPattern("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
