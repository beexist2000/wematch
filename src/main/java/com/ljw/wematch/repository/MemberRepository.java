package com.ljw.wematch.repository;

import com.ljw.wematch.entity.Member;
import com.ljw.wematch.enums.MemberGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Optional<Member> findByUsername(String username);
    Page<Member> findAllByUsernameAndIsEnabledAndMemberGroup(String username, boolean isEnabled, MemberGroup memberGroup, Pageable pageable);

    long countByUsername(String username);
    long countByNickname(String nickname);

    long countByDateCreateGreaterThanEqualAndDateCreateLessThanEqualAndIsEnabled(LocalDateTime dateStart, LocalDateTime dateEnd, boolean isEnabled);
}
