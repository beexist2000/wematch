package com.ljw.wematch.repository;

import com.ljw.wematch.entity.Complaint;
import com.ljw.wematch.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;

public interface ComplaintRepository extends JpaRepository<Complaint, Long> {
    Page<Complaint> findAllByMemberAndDateCreateGreaterThanEqualAndDateCreateLessThanEqual(Member member, LocalDateTime dateStart, LocalDateTime dateEnd, Pageable pageable);
    Page<Complaint> findAllByMember_IsEnabledAndDateCreateGreaterThanEqualAndDateCreateLessThanEqual(boolean memberIsEnabled, LocalDateTime dateStart, LocalDateTime dateEnd, Pageable pageable);

    long countByDateCreateGreaterThanEqualAndDateCreateLessThanEqual(LocalDateTime dateStart, LocalDateTime dateEnd);
}
