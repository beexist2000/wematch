package com.ljw.wematch.repository;

import com.ljw.wematch.entity.Match;
import com.ljw.wematch.entity.MatchApply;
import com.ljw.wematch.entity.Member;
import com.ljw.wematch.enums.AcceptState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MatchApplyRepository extends JpaRepository<MatchApply, Long> {
    Optional<MatchApply> findByMemberAndMatch_IdAndIsEnabled(Member member, long matchId, boolean isEnabled);

    Page<MatchApply> findAllByMatchIdAndIsEnabled(long matchId, boolean isEnabled, Pageable pageable);
    List<MatchApply> findAllByMatchIdAndIsEnabled(long matchId, boolean isEnabled);

    Page<MatchApply> findAllByMemberAndIsEnabledAndDateCreateGreaterThanEqualAndDateCreateLessThanEqual(
            Member member,
            boolean isEnabled,
            LocalDateTime dateStart,
            LocalDateTime dateEnd,
            Pageable pageable
    );

    long countByMatchAndAcceptState(Match match, AcceptState acceptState);
}
