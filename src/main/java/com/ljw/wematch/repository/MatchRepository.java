package com.ljw.wematch.repository;

import com.ljw.wematch.entity.Match;
import com.ljw.wematch.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;

public interface MatchRepository extends JpaRepository<Match, Long> {
    Page<Match> findAllByMemberAndIsEnabledAndDateCreateGreaterThanEqualAndDateCreateLessThanEqual(
            Member member,
            boolean isEnabled,
            LocalDateTime dateStart,
            LocalDateTime dateEnd,
            Pageable pageable
    );

    long countByDateCreateGreaterThanEqualAndDateCreateLessThanEqualAndIsEnabled(LocalDateTime dateStart, LocalDateTime dateEnd, boolean isEnabled);
}
