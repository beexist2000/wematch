package com.ljw.wematch.repository;

import com.ljw.wematch.entity.ComplaintAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ComplaintAnswerRepository extends JpaRepository<ComplaintAnswer, Long> {
    List<ComplaintAnswer> findAllByComplaint_Id(long complaintId);
}
