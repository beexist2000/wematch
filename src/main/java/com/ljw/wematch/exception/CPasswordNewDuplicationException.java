package com.ljw.wematch.exception;

public class CPasswordNewDuplicationException extends RuntimeException {
    public CPasswordNewDuplicationException(String msg, Throwable t) {
        super(msg, t); // 부모의 생성자에 msg, t 값을 인자로 주며 생성
    }

    /**
     * msg를 인자로 필요로 하는 생성자 생성
     *
     * @param msg 예외처리 메시지
     */
    public CPasswordNewDuplicationException(String msg) {
        super(msg); // 부모의 생성자에 msg 값을 인자로 주며 생성
    }

    /**
     * 빈 생성자 생성
     */
    public CPasswordNewDuplicationException() {
        super(); // 부모의 생성자를 생성
    }
}
