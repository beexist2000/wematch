# 스포츠 매칭 관리 API
***
(개인 프로젝트) 회원들의 스포츠 매칭을 관리하는 api
***

### VERSION
```
0.0.1
```

### TECH STACK
```
JAVA 16
SpringBoot 2.7.3
Spring Security - JWT token
Spring Data JPA
PostgreSQL 14
```

### DURATION
```
총 제작일수 : 9일
DB 설계 : 5일
코드 제작 : 4일
```

### 기능
```
스프링 시큐리티 적용(로그인 시 토큰 발급)

* 로그인
 - 관리자 로그인
 - 부관리자 로그인
 - 일반회원 로그인

* 건의사항 관리
 - 건의사항 등록
 - 내 건의사항 리스트 조회
 - 건의사항 리스트 조회
 - 건의사항 상세정보 조회
 - 건의사항 답변 등록
 - 건의사항 답변 리스트 조회

* 경기 관리
 - 경기 등록
 - 내 경기 리스트 조회
 - 경기 리스트 조회
 - 경기 상세정보 조회
 - 경기 수정
 - 경기 취소
 - 경기 참가신청
 - 내 경기 신청 리스트 조회
 - 경기 신청 리스트 조회
 - 경기 신청 상세정보 조회
 - 신청 수락/거절 처리
 - 신청 취소

* 프로필 관리
 - 내 프로필 조회

* 회원 관리
 - 부관리자 생성
 - 일반회원 생성
 - 회원 리스트 조회
 - 회원 정보 수정
 - 회원 비밀번호 수정
 - 내 계정 탈퇴
 - 회원 삭제
```

### 예시
> * 스웨거 전체 화면
>
>![swagger_all](./images/swagger_all.png)

> * 스웨거 로그인
>
>![swagger_login](./images/swagger_login.png)

> * 스웨거 건의사항 관리
>
>![swagger_complaint](./images/swagger_complaint.png)

> * 스웨거 경기 관리
>
>![swagger_match](./images/swagger_match.png)

> * 스웨거 프로필 관리
>
>![swagger_profile](./images/swagger_profile.png)

> * 스웨거 회원 관리
>
>![swagger_member](./images/swagger_member.png)

> * 스웨거 통계 관리
>
>![swagger_statistics](./images/swagger_statistics.png)